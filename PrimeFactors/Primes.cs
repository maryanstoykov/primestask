﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeFactors
{
    static class Primes
    {
        public static int [] GetPrimes(int number)
        {
            List<int> primes = new List<int>();
            
            int prime = 2;// prime counter

            while (number > 1)
            {
                if ((number % prime) == 0)// if number is divisable with prime
                {
                    primes.Add(prime);// add prime to primes list
                    number = number / prime; 
                    prime = 2;// reset prime counter
                }
                else prime++;
            }

            return primes.ToArray<int>();  //return an array of primes
        }
    }
}
