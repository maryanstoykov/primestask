﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeFactors
{
    class Program
    {
        static void Main()
        {
            int number=0;
            string input = "";

            do // handle user input
            {
                Console.Write("Please enter integer number (0<n) n=");
                input = Console.ReadLine();
                try
                {
                    number = int.Parse(input);

                }
                catch (Exception)
                {
                    number = 0;
                }
                finally
                {
                    if (number < 1)
                    {
                        Console.WriteLine("\n*** Invalid input! ***\n");
                    }
                }

            } while (number<1);


            int [] primes = Primes.GetPrimes(number);// get prime factors

            // show results
            Console.Write("\nPrime factors for number {0}: ", number);

            foreach (int prime in primes)
            {
                Console.Write("  {0}",prime);
            }
            Console.WriteLine("\n");

            Console.WriteLine("Press any key...");
            Console.ReadLine();
        }
    }
}
